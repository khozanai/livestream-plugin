<?php

/**
 * @package    block_livestream
 * @package    block_livestream
 * @subpackage block_livestream
 * @copyright  2017 Raymond Mlambo khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 * @author Raymond Mlambo <khozanai@gmail.com>
 *
 */
use stdClass;

/**
 * The main libraries for this plugin
 *
 * @package    block_archivedata
 * @subpackage block_archivedata
 * @copyright  2017 Raymond Mlambo, khozanai@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 */
defined('MOODLE_INTERNAL') || die;

function select_list_course_categories() {
    global $DB;
    $cc = $DB->get_records_sql("SELECT cc.id, cc.name FROM {course_categories} cc JOIN {course} c ON c.category = cc.id WHERE c.category > ?", [2]);
    $select .= '<select id="qual">'
            . '</option>';
    foreach ($cc as $c) {
        $select .= '<option value="' . $cc->id . '">' . $c->name . '</option>';
    }
    return $select .= '</select>';
}
