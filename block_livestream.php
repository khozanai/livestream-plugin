<?php

/**
 * Class block_livestream
 *
 * @package    block_livestream
 * 
 * @author Raymond Mlambo <khozanai@gmail.com>
 * 
 * This plugin will be used in managing livestreams on this system
 */
class block_livestream extends block_base {

    public $query;
    public $result;

    function init() {
        $this->title = get_string('pluginname', 'block_livestream');
    }

    public function specialization() {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_livestream');
            } else {
                $this->title = $this->config->title;
            }

            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_livestream');
            }
        }
    }

    public function get_content() {
        /* @var $CFG type */
        global $cats, $programmes, $CFG, $DB, $COURSE, $USER;

        $capabilities = array(
            'moodle/backup:backupcourse',
            'moodle/category:manage',
            'moodle/course:create',
            'moodle/site:approvecourse',
            'moodle/restore:restorecourse'
        );
        $context = context_course::instance($COURSE->id);
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            return $this->content;
        }
        $this->content = new stdClass;
        if ($hassiteconfig or has_any_capability($capabilities, $context)) {
            // New livestream
            $newstream_url = new moodle_url('/blocks/livestream/new.php', ['courseid' => $this->page->course->id]);
            $this->content->text .= '<i class="fa fa-video-camera" aria-hidden="true"></i> ';
            $this->content->text .= html_writer::link($newstream_url, 'New livestream');
            $this->content->text .= '<br />';

            return $this->content;
        }

        function instance_allow_multiple() {
            return true;
        }

    }

}
