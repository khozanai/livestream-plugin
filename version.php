<?php
/**
 * @author Raymond Mlambo <khozanai@gmail.com>
 * 
 * Version doc for the block_livestream
 */
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015111600;         // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2015111000;         // Requires this Moodle version
$plugin->component = 'block_livestream'; // Full name of the plugin (used for diagnostics)
