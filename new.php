<?php

/**
 * Class block_livestream
 *
 * @package    block_livestream
 * 
 * @author Raymond Mlambo <khozanai@gmail.com>
 * 
 * New livestream
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');

$courseid = required_param('courseid', PARAM_INT);
$course = get_course($courseid);
$context = context_course::instance($course->id);
require_login($course);
$page_url = new moodle_url('/blocks/livestream/new.php', ['courseid' => $course->id]);
$PAGE->set_url($page_url);
$PAGE->set_title('New livestream');
echo $OUTPUT->header();

echo select_list_course_categories();

echo $OUTPUT->footer();








