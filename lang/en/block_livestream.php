<?php

/*
 * 
 * Language file for the plugin
 */

$string['pluginname'] = 'Livestream';
$string['defaulttext'] = 'Livestream';
$string['defaulttitle'] = 'Livestream';
$string['blockstring'] = 'Regenesys livestreams';
$string['headerconfig'] = 'Livestream settings';
$string['descconfig'] = 'Livestream settings';
$string['labelallowhtml'] = 'Livestream Allow HTML';
$string['descallowhtml'] = 'Livestream Allow HTML Settings';
$string['livestream:addinstance'] = 'Add a Livestream block';
$string['livestream:myaddinstance'] = 'Add a new Livestream block to the my moodle page';
